import React, { useState} from 'react';
import { KeyboardAvoidingView, Platform, ScrollView, View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import DateTimePicker from '@react-native-community/datetimepicker';
import {useNavigation} from '@react-navigation/native';
import {Checkbox} from 'react-native-paper';
import Button from '../../components/Button';
import { 
  Container,
  CreateAccountButton,
  CreateAccountButtonText,
  UserAvatar, 
  Header,
  HeaderTitle,
  UserName,
  Title,
} from './styles';

import Avatar from '../../assets/01.jpg';

const Perfil: React.FC = () => { 
const navigation = useNavigation ();


const [date, setDate] = useState(new Date(1598051730000));
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  const onChange = (_event: any, selectedDate: Date) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const showMode = (currentMode: React.SetStateAction<string>) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };
 
  const [isSelected, setSelection] = useState(false);

  
  return (
    <>
   
    <Header>

   <HeaderTitle>
  
   Configuração do Paciente {"\n"}
    <UserName>Nome do Paciente</UserName>
  </HeaderTitle>
  <UserAvatar source={Avatar}/>
  
</Header>
<ScrollView>

<Title>Exercícios MMSS</Title>


    <View style={styles.container}>
      <View style={styles.checkboxInput}>
        <Checkbox
          value={isSelected}
          onValueChange={setSelection}
          style={styles.checkbox}
        />
        <Text style={styles.label}>Deltoide/ Romboide/ 
        Manguito Rotador/{"\n"}Trapézio Desenvolvimento ou
        Extensor {"\n"} de ombro/ Elevação lateral</Text>
      </View>
    
    </View>

    <View style={styles.container}>
      <View style={styles.checkboxInput}>
        <Checkbox
          value={isSelected}
          onValueChange={setSelection}
          style={styles.checkbox}
        />
        <Text style={styles.label}>Peitoral - Abdução e Adução 
         Horizontal {"\n"}de ombro/ Flexão de braço adaptado/  {"\n"}Supino. </Text>
      </View>
    
    </View>

    <View style={styles.container}>
      <View style={styles.checkboxInput}>
        <Checkbox
          value={isSelected}
          onValueChange={setSelection}
          style={styles.checkbox}
        />
        <Text style={styles.label}>Biceps - Rosca direta/Rosca alternada/{"\n"}
        Rosca Martelo.</Text>
      </View>
    
    </View>


    <View style={styles.container}>
      <View style={styles.checkboxInput}>
        <Checkbox
          value={isSelected}
          onValueChange={setSelection}
          style={styles.checkbox}
        />
        <Text style={styles.label}>Triceps - Tríceps Testa/ Supino fechado/{"\n"}
        Tríceps Frances / Tríceps Coice.</Text>
      </View>
    
    </View>




<Title>Exercícios MMII</Title>
<View style={styles.container}>
      <View style={styles.checkboxInput}>
        <Checkbox
          value={isSelected}
          onValueChange={setSelection}
          style={styles.checkbox}
        />
        <Text style={styles.label}>Quadríceps Femoral - Agachamentos{"\n"}(com apoio, senta e levanta) Afundo/
        {"\n"}Flexão de Joelhos/Extensão do Joelho
        </Text>
      </View>
    
    </View>

    <View style={styles.container}>
      <View style={styles.checkboxInput}>
        <Checkbox
          value={isSelected}
          onValueChange={setSelection}
          style={styles.checkbox}
        />
        <Text style={styles.label}>Glúteo e Posterior de Coxa - Abdução {"\n"}
        e Adução de Quadril (com variações {"\n"}deitado, em pé e sentado)</Text>
      </View>
    
    </View>

    <View style={styles.container}>
      <View style={styles.checkboxInput}>
        <Checkbox
          value={isSelected}
          onValueChange={setSelection}
          style={styles.checkbox}
        />
        <Text style={styles.label}>Panturrilha - Panturrilha em Pé/
        Panturrilha {"\n"}Sentado/ Rrotação de Tornozelo/Flexão e {"\n"}Extensão de pé.</Text>
      </View>
    
    </View>



<Title>Exercícios de Fortalecimento{"\n"}
  Core,Tronco, Abdômen </Title>


  <View style={styles.container}>
      <View style={styles.checkboxInput}>
        <Checkbox
          value={isSelected}
          onValueChange={setSelection}
          style={styles.checkbox}
        />
        <Text style={styles.label}>Abdominal Supra/ Infra/ Obliquo{"\n"}Controle de Respiração</Text>
      </View>
    
    </View>


     <KeyboardAvoidingView 
     style={{flex: 1}}
     behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled
      >
        
     <Container>
      
        <Button onPress={showDatepicker}>
          DATA
        </Button>
      
        <Button onPress={showTimepicker} >
          HORÁRIO
        </Button>
      
      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
      )}


<Button onPress={()=>navigation.navigate('Bloco')}
 >
  BLOCO DE NOTAS
  </Button>
  
  <Button onPress={()=>navigation.navigate('Agenda')}
 >
  AGENDAMENTO
  </Button>
  <Button onPress={()=>navigation.navigate('')}>
  EXCLUIR PERFIL
  </Button>


  </Container>


  </KeyboardAvoidingView>
  </ScrollView>
  <CreateAccountButton onPress={()=>navigation.navigate('Contato')}>
    <Icon sname="arrow-left" size={20} color="#126DE8" />
    <CreateAccountButtonText>Contato Pacientes</CreateAccountButtonText>
  </CreateAccountButton>
  </>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  checkboxInput: {
    flexDirection: "row",
  },
  label: {
    color: "#fff",
    margin: 10,
    fontSize: 15, 
  },
});
export default Perfil;