import styled from 'styled-components/native';
import {getStatusBarHeight} from'react-native-iphone-x-helper';
import {getBottomSpace} from 'react-native-iphone-x-helper';

export const Container = styled.View`
flex: 1;
align-items: center;
justify-content: center;
padding: 10px 0 100px;
`;

export const Title = styled.Text`
font-size: 23px;
text-align: center;
color: #f4ede8;
margin:10px;

`;

export const ForgotPasswordText = styled.Text`
color: #f4ede8;
font-size: 16px;

`;

export const CreateAccountButton = styled.TouchableOpacity`
position: absolute;
left: 0;
bottom: 0;
right:0;
background: #fff;
border-top-width: 1px;
border-color: #232129;
padding: 16px 0 ${10 + getBottomSpace()}px;

justify-content: center;
align-items: center;
flex-direction: row;
`;
export const CreateAccountButtonText = styled.Text`
color: #126DE8;
font-size: 18px;
margin-left: 10px;
`;

export const UserAvatar = styled.Image`
width: 60px;
height: 60px;
border-color: #fff;
border-radius: 28px;
`;

export const Header = styled.View`
padding:25px;
padding-top: ${getStatusBarHeight() + 25}px;
background: #126DE8;


flex-direction: row;
justify-content: space-between;
 s
`;

export const HeaderTitle= styled.Text`
color: #f4ede9;
font-size: 20px;
line-height: 28px;
`;

export const UserName= styled.Text`
font-size: 24px;
`;