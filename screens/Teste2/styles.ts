import styled from 'styled-components/native';


export const Container = styled.View`
flex: 1;
align-items: center;
justify-content: center;
padding: 0 40px 70px;
`;

export const Title = styled.Text`
text-align: center;
font-size: 24px;
color: #f4ede8;

margin: 100px 0 20px;
`;
export const Conteudo = styled.Text`
text-align: center;
font-size: 20px;
color: #f4ede8;

margin: 10px 0 20px;
`;


