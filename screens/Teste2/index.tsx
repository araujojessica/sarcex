import React from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Platform, 
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';

import Input from '../../components/Input';
import Button from '../../components/Button';

import { 
  Container,
  Title,
  Conteudo,
} from './styles';

const Teste2: React.FC = () => {
  const navigation=useNavigation();

  return (
    <>
       <StatusBar barStyle="light-content"/>

       <KeyboardAvoidingView  
       style={{ flex: 1 }}
       behavior={Platform.OS === 'ios' ? 'padding' : undefined}
       enabled
      >
       <ScrollView keyboardShouldPersistTaps="handled"
        contentContainerStyle={{ flex:1 }}
       >
       <Container>
       <View>
       <Title>Avaliação Bioimpedância</Title>

       <Conteudo>Exame através de uma balança tecnológica , para análise completa do peso corporal, 
percentual de gordura, de água e de massa muscular.</Conteudo>

       </View> 
      
       <Input name="name" icon="user" placeholder="% Gordura:" />

       <Conteudo>Normal: Homem 18% a 24% {"\n"}Mulher:23% a 31%</Conteudo>

       <Input name="name" icon="user" placeholder="% massa magra:"/>

       <Conteudo>Homem: > 33% Mulher: > 25 %</Conteudo>

       <Button onPress={()=> navigation.navigate('Teste3')}>
         Avançar
       </Button>
       </Container>
       </ScrollView>
       </KeyboardAvoidingView>
 
  </>
  );
};

export default Teste2;