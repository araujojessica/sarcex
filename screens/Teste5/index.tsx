import React from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Platform, 
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';

import Input from '../../components/Input';
import Button from '../../components/Button';

import { 
  Container,
  Title,
  Conteudo,
} from './styles';

const Teste5: React.FC = () => {
  const navigation=useNavigation();

  return (
    <>
       <StatusBar barStyle="light-content"/>

       <KeyboardAvoidingView  
       style={{ flex: 1 }}
       behavior={Platform.OS === 'ios' ? 'padding' : undefined}
       enabled
      >
       <ScrollView keyboardShouldPersistTaps="handled"
        contentContainerStyle={{ flex:1 }}
       >
       <Container>
       <View>
       <Title>Teste de Caminhada {"\n"}de 6 minutos</Title>

       <Conteudo>
       Em um solo plano com extensão entre 20 e 30 metros, solicita-se que o indivíduo caminhe durante seis minutos, em ritmo próprio, com o objetivo de cobrir a maior distância possível, dentro do tempo proposto. Antes e imediatamente depois da conclusão do teste, são aferidas a freqüência cardíaca, a pressão arterial, a oximetria e a intensidade do cansaço, esta última por meio de um questionário padronizado – a escala de Bor Distancia Percorrida: 401,185 –(2,402 x Idade anos) + (43,247 x Gênero  (masc. = 1; fem. = 0) + 1,757 x estatura cm).Escore médio:410,5 metros para os homens e de 371,0 metros para as mulheres.
</Conteudo>

       </View> 
      
       <Input name="name" icon="user" placeholder="Resposta" />
       <Button onPress={()=> navigation.navigate('Contato')}>
      ENVIAR
       </Button>
       </Container>
       </ScrollView>
       </KeyboardAvoidingView>
 
  </>
  );
};

export default Teste5;