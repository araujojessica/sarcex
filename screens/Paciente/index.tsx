import React from 'react';
import {
  Image,
  View,
  ScrollView,
  KeyboardAvoidingView,
  Platform, 
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import Input from '../../components/Input';
import Button from '../../components/Button';
import Avatar from '../../assets/camera.png';


import { 
  Container,
  Title,
  Conteudo,
  UserAvatar,
} from './styles';

const Paciente: React.FC = () => {
  const navigation=useNavigation();
  return (
    <>
       <KeyboardAvoidingView 
       style={{ flex: 1 }}
       behavior={Platform.OS === 'ios' ? 'padding' : undefined}
       enabled
      >
       <ScrollView keyboardShouldPersistTaps="handled"
        contentContainerStyle={{ flex:1 }}
       >
       <Container>
       

       <View>
       <Title>Vamos cadastrar o paciente.</Title>
       </View> 

       <UserAvatar source={Avatar}/>

       <View>
       <Title>Adicionar foto Perfil Paciente</Title>
       </View> 

       <Input name="name" icon="user" placeholder="Nome Completo" />
       <Input name="phone" icon="phone" placeholder="Telefone"/>
       <Input name="calendar" icon="calendar"placeholder="Idade ou data de nascimento"/>

       <View>
       <Conteudo>Todo Paciente Novo tem que passar pelo teste Sarcopenia</Conteudo>
       </View>

       <Button onPress={()=> navigation.navigate('Info')}>
       Teste para diagnostico
       </Button>
       </Container>
       </ScrollView>
       </KeyboardAvoidingView>
 
  </>
  );
};

export default Paciente;