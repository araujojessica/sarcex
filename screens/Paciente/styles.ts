import styled from 'styled-components/native';

export const Container = styled.View`
flex: 1;
align-items: center;
justify-content: center;
padding: 70px 30px;
`;

export const Title = styled.Text`
font-size: 24px;
text-align: center;
color: #f4ede8;

margin: 10px 0 10px;
`;

export const Conteudo = styled.Text`
font-size: 18px;
text-align: center;
color: #f4ede8;

margin: 10px 0 10px;
`;

export const UserAvatar = styled.Image`
width: 150px;
height: 150px;

align-items: center;
background: #fff;
border-color: #fff;
border-radius: 100px;
`;
