import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import {KeyboardAvoidingView, ScrollView, Platform} from 'react-native'
import {useNavigation} from '@react-navigation/native';
import {
  Container, 
  Header,
  HeaderTitle,
  UserName,
  ProfileButton,
  List,
  ListContainer,
  ListName,
  ListAvatar,
  ListMeta,
  ListInfo, 
  } from './style';


import Paciente01 from '../../assets/01.jpg';
import Paciente02 from '../../assets/02.jpg';

const Contato: React.FC = () => {
  const navigation = useNavigation();
 return( 
<>
<Container>

  <Header>

    <HeaderTitle>
        
      <UserName>
        
        
    <Icon onPress={()=>navigation.navigate('Agenda')} name="arrow-left" 
    size={27} color="#fff" />Contatos Pacientes
    

    </UserName>
    
     </HeaderTitle>
  
  </Header>
  <KeyboardAvoidingView 
       style={{ flex: 1 }}
       behavior={Platform.OS === 'ios' ? 'padding' : undefined}
       enabled
      >
        <ScrollView >
  <List>

  <ListContainer>
  <ListAvatar source={Paciente01}/>
   <ListMeta>

    <ListInfo>
    <ListName onPress={()=>navigation.navigate('Perfil')}>
    Nome do Paciente
      
      </ListName>
    </ListInfo>

    </ListMeta>
   </ListContainer>

   <ListContainer>
  <ListAvatar source={Paciente02}/>
   <ListMeta>

    <ListInfo>
    <ListName onPress={()=>navigation.navigate('Perfil')}>
    Nome do Paciente
      
      </ListName>
    </ListInfo>

    </ListMeta>
   </ListContainer>

   <ListContainer>
  <ListAvatar source={Paciente01}/>
   <ListMeta>

    <ListInfo>
    <ListName>
    Nome do Paciente
      
      </ListName>
    </ListInfo>

    </ListMeta>
   </ListContainer>


   <ListContainer>
  <ListAvatar source={Paciente02}/>
   <ListMeta>

    <ListInfo>
    <ListName>
    Nome do Paciente
      
      </ListName>
    </ListInfo>

    </ListMeta>
   </ListContainer>

   <ListContainer>
  <ListAvatar source={Paciente01}/>
   <ListMeta>

    <ListInfo>
    <ListName>
    Nome do Paciente
      
      </ListName>
    </ListInfo>

    </ListMeta>
   </ListContainer>
   <ListContainer>
  <ListAvatar source={Paciente02} />
   <ListMeta>

    <ListInfo>
    <ListName>
    Nome do Paciente
      
      </ListName>
    </ListInfo>

    </ListMeta>
   </ListContainer>

  </List>

</ScrollView>
</KeyboardAvoidingView> 
   </Container>
</>
 );

};
export default Contato;