import styled from 'styled-components/native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';
import {getBottomSpace} from 'react-native-iphone-x-helper';

export const Container = styled.View`
flex: 1;
background-color: #fff;
`;

export const Header = styled.View`
padding:30px;
padding-top: ${getStatusBarHeight() + 24}px;
background: #126DE8;

flex-direction: row;
justify-content: space-between;
align-items: center;
`;

export const HeaderTitle= styled.Text`
color: #f4ede9;
font-size: 20px;
line-height: 28px;
`;

export const UserName= styled.Text`

font-size: 24px;
`;

export const ProfileButton = styled.TouchableOpacity`
`;
export const UserAvatar = styled.Image`
width: 56px;
height: 56px;
border-color: #fff;
border-radius: 28px;
`;

export const Text = styled.Text`

font-size: 24px;
color: #126DE8;
text-align: center;
margin-top: 20px;
`;
export const List = styled.View`
padding-top:50;
padding-left:20;
padding-right:20;
`;

export const ListContainer =  styled.View`
background: #126DE8;
border-radius: 20px;
padding: 20px;
margin-bottom: 16px;
flex-direction: row;
align-items: center;

`;
export const ListAvatar = styled.Image`
width: 72px;
height: 72px;
border-radius: 36px;
`;

export const ListInfo = styled.View`
flex:1;
margin-left: 20px;
`;

export const ListName = styled.Text`

font-size: 18px;
color: #fff;
`;

export const ListMeta = styled.View`
flex-direction: row;
align-items: center;
margin-top: 10px;
`;

export const ListMetaText = styled.View`
margin-left: 8px;
color: #999591;
`;

export const CreateAccountButton = styled.TouchableOpacity`
position: absolute;
left: 0;
bottom: 0;
right:0;
border-top-width: 1px;
border-color: #232129;
padding: 16px 0 ${10 + getBottomSpace()}px;

justify-content: center;
align-items: center;
flex-direction: row;
`;
