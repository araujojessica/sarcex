import styled from 'styled-components/native';

export const Container = styled.View`
flex: 1;
background: #fff;
align-items: center;
justify-content: center;
padding: 70px 30px;
`;

export const Title = styled.Text`
font-size: 24px;
text-align: center;
color: #000;

margin: 20px 0 30px;
`;
export const Conteudo = styled.Text`
font-size: 15px;
text-align: center;
color: #000;

margin: 20px 0 30px;
`;


