import React from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Platform, 
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';
import Button from '../../components/Button';

import { 
  Container,
  Title,
  Conteudo,
} from './styles';

const Info: React.FC = () => {
  const navigation=useNavigation();
  return (
    <>
       <StatusBar barStyle="dark-content"/>

       <KeyboardAvoidingView 
       style={{ flex: 1 }}
       behavior={Platform.OS === 'ios' ? 'padding' : undefined}
       enabled
      >
        <ScrollView keyboardShouldPersistTaps="handled"
        contentContainerStyle={{ flex:1 }}
       >
       <Container>
      
       <View>
       <Title>INFORMAÇÃO</Title>
       <Title>Sarcopenia, queda do desempenho muscular, massa, força, potência e resistência que pode ser confirmado através testes, que devem ser realizados para uma melhor confirmação diagnostica do paciente. <Conteudo>(DODDS e SAYER, 2014)</Conteudo> </Title>
       </View> 
       <Button onPress={()=> navigation.navigate('Teste')}>
       Começar o teste
       </ Button>
       </Container>
       </ScrollView>
       </KeyboardAvoidingView>
 
  </>
  );
};

export default Info;