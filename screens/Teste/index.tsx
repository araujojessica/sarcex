import React from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Platform, 
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';

import Input from '../../components/Input';
import Button from '../../components/Button';

import { 
  Container,
  Title,
  Conteudo
} from './styles';

const Teste: React.FC = () => {
  const navigation=useNavigation();

  return (
    <>
       <StatusBar barStyle="light-content"/>
       <KeyboardAvoidingView 
       style={{ flex: 1 }}
       behavior={Platform.OS === 'ios' ? 'padding' : undefined}
       enabled
      >
       <ScrollView >
       <Container>
       <View>
       <Title>Questionário Clinico</Title>
       </View> 
       <View>
       <Title>Dados Clínicos:</Title>
       </View> 
       <Conteudo>Doenças?</Conteudo>
       <Input name="name" icon="user" placeholder="Quais?" />
       <Conteudo>Medicamentos uso contínuo?</Conteudo>
       <Input name="name" icon="user" placeholder="Quais?"/>
       <Conteudo>Tabagismo?</Conteudo>
       <Input name="name" icon="user" placeholder=" Sim ou Não"/>
       <Conteudo>Pratica Atividade Física?</Conteudo>
       <Input name="name" icon="user" placeholder="Sim ou Não"/>

       <View>
       <Title></Title>
       </View>

       <Button onPress={()=> navigation.navigate('Teste2')}>
         Avançar
       </Button>
       </Container>
       </ScrollView>
       </KeyboardAvoidingView>
 
  </>
  );
};

export default Teste;