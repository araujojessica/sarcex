import styled from 'styled-components/native';
import {getBottomSpace} from 'react-native-iphone-x-helper';

export const Container = styled.View`
flex: 1;
align-items: center;
justify-content: center;
padding: 0 40px 90px;
`;

export const Title = styled.Text`
text-align: center;
font-size: 24px;
color: #f4ede8;

margin: 100px 0 20px;
`;
export const Conteudo = styled.Text`
text-align: center;
font-size: 20px;
color: #f4ede8;

margin: 1px 0 20px;
`;

export const BackToSignIn = styled.TouchableOpacity`
position: absolute;
left: 0;
bottom: 0;
right:0;
background: #fff;
border-top-width: 1px;
border-color: #232129;
padding: 16px 0 ${10 + getBottomSpace()}px;

justify-content: center;
align-items: center;
flex-direction: row;
`;
