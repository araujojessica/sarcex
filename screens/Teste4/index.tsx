import React from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Platform, 
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';

import Input from '../../components/Input';
import Button from '../../components/Button';

import { 
  Container,
  Title,
  Conteudo,
} from './styles';

const Teste4: React.FC = () => {
  const navigation=useNavigation();

  return (
    <>
       <StatusBar barStyle="light-content"/>

       <KeyboardAvoidingView  
       style={{ flex: 1 }}
       behavior={Platform.OS === 'ios' ? 'padding' : undefined}
       enabled
      >
       <ScrollView keyboardShouldPersistTaps="handled"
        contentContainerStyle={{ flex:1 }}
       >
       <Container>
       <View>
       <Title>Teste Time up and GO (TUG)</Title>

       <Conteudo>
Teste consiste em levantar- se de uma cadeira sem a ajuda dos braços e andar
em ritmo confortável e seguro a uma distância de três metros, dar a volta, 
retornar e sentar novamente, cronometrado em segundos. Para análise do desempenho o escore:  {">"} 8,1 s Indicativo para Sarcopenia.
</Conteudo>

       </View> 
      
       <Input name="name" icon="user" placeholder="Resposta" />
       <Button onPress={()=> navigation.navigate('Teste5')}>
      Avançar
       </Button>
       </Container>
       </ScrollView>
       </KeyboardAvoidingView>
 
  </>
  );
};

export default Teste4;