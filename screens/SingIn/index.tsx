import React from 'react';

import { Image, View, ScrollView ,KeyboardAvoidingView, Platform, } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';

import Input from '../../components/Input';
import Button from '../../components/Button';

import logoImg from '../../assets/logo.png';

import { 
  Container,
  Title,
  ForgotPassword,
  ForgotPasswordText,
  CreateAccountButton,
  CreateAccountButtonText
} from './styles';

const SingIn: React.FC = () => {
const navigation = useNavigation ();


  return (
    <>
     <KeyboardAvoidingView 
     style={{flex: 1}}
     behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled
      >
        <ScrollView >
     <Container>
     <Image source={logoImg} />

       <View>
       
       <Title>Faça seu login</Title>
       </View> 

      <Input name="email" icon="mail" placeholder="E-mail" />
      <Input name="password" icon="lock" placeholder="Senha"/>
      

      <Button onPress={()=>navigation.navigate('Agenda')}> 
  ENTRAR
  </Button>
  <ForgotPassword onPress={() => {}}>
    <ForgotPasswordText>Esqueci minha senha</ForgotPasswordText>
  </ForgotPassword>
  </Container>
  </ScrollView>
  </KeyboardAvoidingView>
 
  <CreateAccountButton onPress={()=>navigation.navigate('SingUp')}>
    <Icon name="log-in" size={20} color="#126DE8" />
    <CreateAccountButtonText>Cadastro para Profissionais</CreateAccountButtonText>
  </CreateAccountButton>
  </>
  );
};

export default SingIn;