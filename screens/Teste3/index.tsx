import React from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Platform, 
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';

import Input from '../../components/Input';
import Button from '../../components/Button';

import { 
  Container,
  Title,
  Conteudo,
} from './styles';

const Teste3: React.FC = () => {
  const navigation=useNavigation();

  return (
    <>
       <StatusBar barStyle="light-content"/>

       <KeyboardAvoidingView  
       style={{ flex: 1 }}
       behavior={Platform.OS === 'ios' ? 'padding' : undefined}
       enabled
      >
       <ScrollView keyboardShouldPersistTaps="handled"
        contentContainerStyle={{ flex:1 }}
       >
       <Container>
       <View>
       <Title>Avaliação Antropométrica
       (peso, altura, circunferência panturrilha )</Title>

       <Conteudo>IMC: Peso corporal em quilogramas e a altura em metros elevada ao quadrado (IMC= peso/altura²) baixo peso(maior 18,5 kg/m2), peso normal (18,5-24,9 kg/m2), sobrepeso (25-29,9kg/m2)  obesos ({">"} 30 kg/m2).13,14
       Circunferência Panturrilha mensurar a maior porção da região da panturrilha sem comprimi-la.
       Um valor inferior a 31 centímetro1s indicado depleção de massa muscular.</Conteudo>

       </View> 
      
       <Input name="name" icon="user" placeholder="% Resultado:" />


       <Button onPress={()=> navigation.navigate('Teste4')}>
         Avançar
       </Button>
       </Container>
       </ScrollView>
       </KeyboardAvoidingView>
 
  </>
  );
};

export default Teste3;