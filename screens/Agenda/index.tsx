import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';
import {KeyboardAvoidingView, ScrollView, Platform} from 'react-native';
import{
  Container, 
  Header,
  HeaderTitle,
  UserName,
  UserAvatar,
  List,
  ListContainer,
  ListName,
  ListAvatar,
  ListMeta,
  ListInfo, 
  Text,
  BackToSignIn,
  BackToSignInText,
  } from './style';

import Paciente01 from '../../assets/01.jpg';
import Paciente02 from '../../assets/02.jpg';

import Avatar from '../../assets/noeli.jpg';

const Agenda: React.FC = () => {
  const navigation = useNavigation();
 return( 
<>
<Container>

  <Header>

    <HeaderTitle>
      
      Seja Bem-Vinda! {"\n"}
      <UserName onPress={()=> navigation.navigate('Perfilpro')}>Noeli</UserName>{"\n"}
      
      </HeaderTitle>

      <UserAvatar source={Avatar} 
      onPress={()=> navigation.navigate('Perfilpro')}/>

      
  </Header>

<Text >
  Atendimento do dia
</Text>

<KeyboardAvoidingView 
       style={{ flex: 1 }}
       behavior={Platform.OS === 'ios' ? 'padding' : undefined}
       enabled
      >
        <ScrollView >
         
  <List>
<ListContainer>
  <ListAvatar source={Paciente01} />
  <ListMeta>

    <ListInfo>
    <ListName onPress={()=>navigation.navigate('Perfil')}>Nome do Paciente</ListName>
    <ListName>Exercícios físicos</ListName>
    <ListName><Icon name="calendar"size={18} />10/03/2021</ListName>
    <ListName><Icon name="clock"size={18} />08:00-09:00</ListName>
  </ListInfo>

  </ListMeta>
</ListContainer>


<ListContainer>
  <ListAvatar source={Paciente02}/>
  <ListMeta>

    <ListInfo>
    <ListName>Nome do Paciente</ListName>
    <ListName>Exercícios físicos</ListName>
    <ListName><Icon name="calendar" size={18}/>11/03/2021</ListName>
    <ListName><Icon name="clock"size={18} />10:00-11:00</ListName>
    </ListInfo>

  </ListMeta>
</ListContainer>
  
<ListContainer>
  <ListAvatar source={Paciente01}/>
  <ListMeta>

    <ListInfo>
    <ListName>Nome do Paciente</ListName>
    <ListName>Exercícios físicos</ListName>
    <ListName><Icon name="calendar" size={18}/>12/03/2021</ListName>
    <ListName><Icon name="clock"size={18} />10:00-11:00</ListName>
    </ListInfo>

  </ListMeta>
</ListContainer>

<ListContainer>
  <ListAvatar source={Paciente02}/>
  <ListMeta>

    <ListInfo>
    <ListName>Nome do Paciente</ListName>
    <ListName>Exercícios físicos</ListName>
    <ListName><Icon name="calendar" size={18}/>13/03/2021</ListName>
    <ListName><Icon name="clock"size={18} />10:00-11:00</ListName>
    </ListInfo>

  </ListMeta>
</ListContainer>

<ListContainer>
  <ListAvatar source={Paciente01}/>
  <ListMeta>

    <ListInfo>
    <ListName>Nome do Paciente</ListName>
    <ListName>Exercícios físicos</ListName>
    <ListName><Icon name="calendar" size={18}/>14/03/2021</ListName>
    <ListName><Icon name="clock"size={18} />10:00-11:00</ListName>
    </ListInfo>

  </ListMeta>
</ListContainer>

<ListContainer>
  <ListAvatar source={Paciente02} />
  <ListMeta>

    <ListInfo>
    <ListName>Exercícios físicos</ListName>
    <ListName>Nome do Paciente</ListName>
    <ListName><Icon name="calendar" size={18}/>Terça-Feira</ListName>
    <ListName><Icon name="clock"size={18} />10:00-11:00</ListName>
    </ListInfo>

  </ListMeta>
</ListContainer>
</List>


</ScrollView>

        <BackToSignIn onPress={()=> navigation.navigate("Paciente")}>
        <Icon name="user" size={30} color="#126DE8" />
        <BackToSignInText>ADICIONAR NOVO PACIENTE</BackToSignInText>
        </ BackToSignIn>

</KeyboardAvoidingView>
</Container>
</>
 );
};
export default Agenda;