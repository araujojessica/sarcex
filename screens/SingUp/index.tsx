import React from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Platform, 
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';

import Input from '../../components/Input';
import Button from '../../components/Button';
import Avatar from '../../assets/camera.png';

import { 
  Container,
  Title,
  BackToSignIn,
  BackToSignInText, 
  UserAvatar,
} from './styles';

const SingUp: React.FC = () => {
  const navigation= useNavigation();
  return (
    <>
       <KeyboardAvoidingView 
       style={{ flex: 1 }}
       behavior={Platform.OS === 'ios' ? 'padding' : undefined}
       enabled
      >
        <ScrollView >
         
       <Container>
       <View>
       <Title>Crie sua conta</Title>
       </View> 
       
       <UserAvatar source={Avatar}/>

       <View>
       <Title>Adicionar sua foto de Perfil</Title>
       </View> 

       <Input name="name" icon="user" placeholder="Nome Completo" />
       <Input name="phone" icon="phone" placeholder="Telefone"/>
       <Input name="email" icon="mail" placeholder="E-mail" />
       <Input name="password" icon="lock" placeholder="Senha"/>
      

       <Button onPress={()=> navigation.navigate("Paciente")}>
       ENVIAR
       </Button>
       </Container>
       </ScrollView>
       </KeyboardAvoidingView>
 
        <BackToSignIn onPress={()=> navigation.goBack('SingIn')}>
        <Icon name="arrow-left" size={20} color="#126DE8" />
        <BackToSignInText>Voltar para Login</BackToSignInText>
        </ BackToSignIn>
  </>
  );
};

export default SingUp;