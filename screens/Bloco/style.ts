import styled from 'styled-components/native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';
import {getBottomSpace} from 'react-native-iphone-x-helper';

export const Container = styled.View`
flex: 1;
background-color: #fff;
`;

export const Header = styled.View`
padding:30px;
padding-top: ${getStatusBarHeight() + 24}px;
background: #126DE8;

flex-direction: row;
justify-content: space-between;
align-items: center;
`;

export const HeaderTitle= styled.Text`
color: #f4ede9;
font-size: 20px;
line-height: 28px;
`;

export const UserName= styled.Text`
font-size: 24px;
`;


export const UserAvatar = styled.Image`
width: 56px;
height: 56px;
border-color: #fff;
border-radius: 28px;
`;

export const Text = styled.Text`
font-size: 24px;
color: #126DE8;
text-align: center;
margin-top: 20px;
`;


export const CreateAccountButton = styled.TouchableOpacity`
position: absolute;
left: 0;
bottom: 0;
right:0;
border-top-width: 1px;
border-color: #232129;
padding: 16px 0 ${10 + getBottomSpace()}px;

justify-content: center;
align-items: center;
flex-direction: row;
`;

export const TextInput = styled.TextInput`
padding: 10px;
flex: 1;
color: #126DE8;
font-size: 20px;
`;
