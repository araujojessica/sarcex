import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import {KeyboardAvoidingView, ScrollView, Platform } from 'react-native'
import {useNavigation} from '@react-navigation/native';
import {
  Container, 
  Header,
  HeaderTitle,
  UserName,
  TextInput
  } from './style';


const Bloco: React.FC = () => {
  const navigation = useNavigation();
 return( 
<>
<Container>

  <Header>

    <HeaderTitle>
        
      <UserName>
        
        
    <Icon onPress={()=>navigation.navigate('Perfil')} name="arrow-left" 
    size={27} color="#fff" />Bloco de Notas
    

    </UserName>
    
     </HeaderTitle>
  
  </Header>
  <KeyboardAvoidingView 
       style={{ flex: 1 }}
       behavior={Platform.OS === 'ios' ? 'padding' : undefined}
       enabled
      >
        <ScrollView >
  

        <TextInput
          placeholder="Digite suas Notas"
          keyboardAppearance="light"
          placeholderTextColor="#666360"
        />
 


</ScrollView>
</KeyboardAvoidingView> 
   </Container>
</>
 );

};
export default Bloco;