import React from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Platform, 
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import Button from '../../components/Button';
import Avatar from '../../assets/noeli.jpg';
import Icon from 'react-native-vector-icons/Feather';

import { 
  Container,
  Title,
  Conteudo,
  UserAvatar,
} from './styles';

const Perfilpro: React.FC = () => {
  const navigation=useNavigation();
  return (
    <>
       <KeyboardAvoidingView 
       style={{ flex: 1 }}
       behavior={Platform.OS === 'ios' ? 'padding' : undefined}
       enabled
      >
       <ScrollView keyboardShouldPersistTaps="handled"
        contentContainerStyle={{ flex:1 }}
       >
       <Container>
       


       <Title><Icon onPress={()=>navigation.navigate('Agenda')} name="arrow-left" 
    size={27} color="#fff" /> Perfil Proficional</Title>

       <UserAvatar source={Avatar}/>

       <View>
       <Title>Trocar de foto</Title>
       </View> 

      

       <View>
       <Conteudo></Conteudo>
       </View>

       <Button onPress={()=> navigation.navigate('Referencia')}>
       Sobre SarcEx
       </Button>
       <Button onPress={()=> navigation.navigate('SingIn')}>
        Fazer LogOff
       </Button>

       <Button onPress={()=> navigation.navigate('Referencia')}>
     Excluir Perfil
       </Button>

       <View>
       <Conteudo>Versão 1.0</Conteudo>
       </View>

       </Container>
       </ScrollView>
       </KeyboardAvoidingView>
 
  </>
  );
};

export default Perfilpro;