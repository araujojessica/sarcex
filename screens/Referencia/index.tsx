import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';
import {KeyboardAvoidingView, ScrollView, Platform} from 'react-native';
import{
  Container, 
  Header,
  HeaderTitle,
  Text
  } from './style';

const Referencia : React.FC = () => {
  const navigation = useNavigation();
 return( 
<>
<Container>

  <Header>

    <HeaderTitle onPress={()=> navigation.goBack('Perfil')}>
    <Icon name="arrow-left" size={23} color="#fff" /> 
    Referencia
    </HeaderTitle>
      
  </Header>
  <KeyboardAvoidingView 
       style={{ flex: 1 }}
       behavior={Platform.OS === 'ios' ? 'padding' : undefined}
       enabled>
        <ScrollView >
       
<Text>
Arai H, Wakabayashi H, Yoshimura Y, Yamada M, Hunkyung Kim H, Harada A. Treatment of Sarcopenia. Geriatr Gerontol Int 2018; 18 (Suppl. 1): 28–44. doi: 10.1111/ggi.13322.
</Text>

<Text>
Dodds R, Sayer AA. Sarcopenia. Arq Bras Endocrinol Metab. 2014, vol.58, n.5, pp.464-469. ISSN 1677-9487, doi 10.1590/0004-273000000.
</Text>

<Text>
Jesus ITM, Orlandi AAS, Grazziano ES, Zazzetta MS. Fragilidade de Idosos em Vulnerabilidade Social. Acta Paul Enferm.2017; 30(6):614-20, doi 10.1590/1982- 0194201700088.
</Text>
<Text>
Peral JAR, Josa MSG. Ejercicios de Resistencia en el Tratamiento y Prevención de la Sarcopenia en Ancianos. Revisión sistemática. Gerokomos. 2018; 29(3):133-137.
</Text>

<Text>
Cardoso RM, Neto JRC, Freitas LPR, Ferreira MPP. Exercício Resistido frente à Sarcopenia: Uma Alternativa Eficaz para a Qualidade de Vida do Idoso. Universidade Presidente Antônio Carlos, UNIPAC. EFDeportes.com, Revista Digital. Buenos Aires - Año 17 - Nº 169 - Junio de 2012.
</Text>

<Text>
Chan DC, Chang CB,  Han DS, Hong CH, Hwang JS, Tsai KS, Yang RS. Effects of Exercise Improves Muscle Strength and fat Mass in Patients With High Fracture Risk: A randomized Control Trial. Journal of the Formosan Medical Association (2018) 117, 572e582.
</Text>

<Text>
Chica A, González-guirval F, Reigal RE, Carranque G, Hernández-mendo A. Efectos de un programa de danza española en mujeres con fibromialgia . Cuadernos de Psicología del Deporte, Vol 19(2) 2019, 52-69. 
</Text>

<Text>
Paula JÁ, Wamser EL, Gomes ARS, Valderramas SR, Cardoso Neto J, Schieferdecke RMEM. Análise de Métodos para Detectar Sarcopenia em Idosas Independentes da Comunidade. Rev. Bras. Geriatr. Gerontol., Rio de Janeiro, 2016, doi10.1590/1809-98232016019.140233.
</Text>

<Text>
Conwright CMD, Courneya KS, Wahnefried WD, Sami N, Lee K, Buchanan TA, Spice DV. Tripathy.D.,Bernstein.L.,Mortimer.J.E. Effects of Aerobic and Resistance Exercise on Metabolic Syndrome, Sarcopenic Obesity, and Circulating Biomarkers in Overweight or Obese Survivors of Breast Cancer: A Randomized Controlled Trial. Journal of clinical oncology, volume 36, number 9, march 20, 2018,doi 10.1200/JCO.2017. 75.7526. 
</Text>

<Text>
Cusumano AM. Sarcopenia en Pacientes con y sin Insuficiencia Renal Crónica: Diagnóstico, Evaluación y Tratamiento. Instituto de Nefrología Pergamino. Nefrología, Diálisis y Trasplante 2015; 35 (1) Pág. 32 a 43.  
</Text>

<Text>
Damanti S, Azzolino D, Roncaglione C, Arosio B, Rossi P, Cesari M. Efficacy of Nutritional Interventions as Stand-Alone or Synergistic Treatments with Exercise for the Management of Sarcopenia. Nutrients 2019, 11, 1991; doi: 10.3390/nu11091991.
</Text>

<Text>
Distefano G, Goodpaster BH. Effects of Exercise and Aging on Skeletal Muscle.  Cold Spring Harb Perspect Med 2018; doi: 10.1101/cshperspect.a029785.
</Text>

<Text>
Teixeira VON, Filippin LI, Xavier RM. Mecanismos de Perda Muscular da Sarcopenia. Rev Bras Reumatol 2012;52(2):247-259.
</Text>
<Text>
Raman M, Loza AJM, Eslamparast T, Tandon P. Sarcopenic Obesity in Cirrhosis; The Confluence of 2 Prognostic Titans. Liver International. 2018;38:1706–1717. DOI: 10.1111/liv.13876.
</Text>

<Text>
Yamada M, Kimura Y, Ishiyama D, Nishio N, Otobe Y, Tanaka T, Ohji S,  Koyama S, Sato A, Suzuki M, Ogawa H, Ichikawa T, Arai H. Synergistic Effect of Bodyweight Resistance Exercise and Protein Supplementation on Skeletal Muscle in Sarcopenic or Dynapenic Older Adults. Epidemiology, clinical practice and health 2019.
</Text>

<Text>
Ruiz MER, Lans VG, Torres IP, Soto ME. Mechanisms Underlying Metabolic Syndrome-Related Sarcopenia and Possible Therapeutic Measures. Int. J. Mol. Sci. 2019, 20, 647; doi:10.3390/ijms20030647.
</Text>

<Text>
Ruiz MER, Lans VG, Torres IP, Soto ME. Mechanisms Underlying Metabolic Syndrome-Related Sarcopenia and Possible Therapeutic Measures. Int. J. Mol. Sci. 2019, 20, 647; doi:10.3390/ijms20030647.
</Text>
<Text>
Torre AM. El Músculo, Elemento Clave para la Supervivencia en el Enfermo Neoplásico Muscle Wasting as a key Predictor of Survival in Cancer Patients. Nutr Hosp 2016;33(Supl. 1):11-16.
</Text>

<Text>
Montoro MVP, Montilla JAP, Aguilera ELA, Checa MA. Intervención en la Sarcopenia con Entrenamiento de Resistencia Progresiva y Suplementos Nutricionales Protéicos. Nutr Hosp. 2015;31(4):1481-1490 ISSN 0212-1611, coden nuhoeq S.V.R. 31.
</Text>

<Text>
Trouwborst I, Verreijen A, Memelink R, Massanet P, Boirie Y, Weijs P, Tieland M. Exercise and Nutrition Strategies to Counteract Sarcopenic Obesity. Nutrients 2018, 10, 605; doi:10.3390/nu10050605.
</Text>

<Text>
Izquierdo M. Prescripción de ejercicio físico. El Programa Vivifrail como modelo Multicomponent Physical Exercise Program: Vivifrail. Nutr Hosp 2019;36(N.º Extra 2):50-56 DOI: 10.20960/nh.02680.
</Text>

<Text>
Kellyo J. Gilman JC, Boschiero D, Ilich, JZ.  Osteosarcopenic Obesity: Current Knowledge, Revised Identification Criteria and Treatment Principles. Nutrients 2019, 11, 747; doi: 10.3390/nu11040747.
</Text>
<Text>
Beaudart C, Zaaria M, Pasleau F, Reginster JY, Bruyère O. Health Outcomes of Sarcopenia: A Systematic Review and Meta-Analysis. PLOS ONE journal.pone.0169548 January 17, 2017 DOI:10.1371.
</Text>

<Text>
Becerro JFM. El entrenamiento de fuerza en los deportistas mayores. Arch Med Deporte 2016; 33(5): 332-337.
</Text>

<Text>
Beaudart C, Zaaria M, Pasleau F, Reginster JY, Bruyère O. Health Outcomes of Sarcopenia: A Systematic Review and Meta-Analysis. PLOS ONE journal.pone.0169548 January 17, 2017 DOI:10.1371.
</Text>

<Text>
Kelly OJ, Gilman JC, Boschiero D, Ilich JZ. Osteosarcopenic Obesity: Current Knowledge, Revised Identification Criteria and Treatment Principles. Nutrients 2019, 11, 747; doi:10.3390/nu11040747.
</Text>

<Text>
Becerro JFM. A Equipe Multiprofissional em Gerontologia e a Produção do Cuidado: um estudo de caso. KAIRÓS. 2014;17(2):205-222.
</Text>

</ScrollView>
</KeyboardAvoidingView>
</Container>
        
</>
 );
};
export default Referencia;