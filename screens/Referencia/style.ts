import styled from 'styled-components/native';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';

export const Container = styled.View`
flex: 1;
background-color: #fff;
`;

export const Header = styled.View`
padding: 10px 0 ${getBottomSpace() + 3}px;
padding:20px;
padding-top: ${getStatusBarHeight() + 20}px;
background: #126DE8;

flex-direction: row;
justify-content: space-between;
align-items: center;
`;

export const HeaderTitle= styled.Text`
color: #f4ede9;
font-size: 20px;
line-height: 20px;
`;

export const Text = styled.Text`

text-align: justify;
font-size: 13px;
color: #000;
padding: 3px 15px;
margin: 05px;
`;

export const BackToSignIn = styled.TouchableOpacity`
position: absolute;
left: 0;
bottom: 0;
right:0;
background: #126DE8;
border-top-width: 1px;
border-color: #232129;

justify-content: center;
align-items: center;
flex-direction: row;
`;
export const BackToSignInText = styled.Text`
color: #fff;
font-size: 18px;
margin-left: 10px;
`;