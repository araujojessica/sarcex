import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import SingIn from '../screens/SingIn';
import SingUp from '../screens/SingUp';
import Paciente from '../screens/Paciente';
import Info from '../screens/Info';
import Teste from '../screens/Teste';
import Teste2 from '../screens/Teste2';
import Teste3 from '../screens/Teste3';
import Teste4 from '../screens/Teste4';
import Teste5 from '../screens/Teste5';
import Agenda from '../screens/Agenda';
import Contato from '../screens/Contato';
import Referencia from '../screens/Referencia';
import Perfil from '../screens/Perfil';
import Bloco from '../screens/Bloco';
import Perfilpro from '../screens/Perfilpro';

const Auth = createStackNavigator();
const AuthRoutes: React.FC = () => (

  <Auth.Navigator
  screenOptions={{ 
  headerShown: false,
  cardStyle: { backgroundColor: '#126DE8'},
  }}
initialRouteName=""
  >
    <Auth.Screen name="SingIn"component={SingIn} />
    <Auth.Screen name="SingUp" component={SingUp} />
    <Auth.Screen name="Paciente"component={Paciente} />
    <Auth.Screen name="Info" component={Info} />
    <Auth.Screen name="Teste" component={Teste} />
    <Auth.Screen name="Teste2" component={Teste2} />
    <Auth.Screen name="Teste3" component={Teste3} />
    <Auth.Screen name="Teste4" component={Teste4} />
    <Auth.Screen name="Teste5" component={Teste5} />
    <Auth.Screen name="Agenda"component={Agenda} />
    <Auth.Screen name="Contato" component={Contato} />
    <Auth.Screen name="Referencia"component={Referencia} />
    <Auth.Screen name="Perfil"component={Perfil} />
    <Auth.Screen name="Bloco"component={Bloco} />
    <Auth.Screen name="Perfilpro" component={Perfilpro} />

    
  </Auth.Navigator>

  
);

export default AuthRoutes;